/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;
import java.util.Random;
/**
 *Esta clase modela un número entero más grande que un long
 * @author Juan Camilo Guarin solano
 * @author Marvin Julian Gonzalez Burbano
 */
public class EnteroGrande {
    private int vector[];
    public EnteroGrande() {
    }
    
    public int[] getVector() {
        return vector;
    }

    public void setVector(int[] vector) {
        this.vector = vector;
    }
    
    public EnteroGrande(int n){
        if(n<=0){
            throw new RuntimeException("Error, no se pueden crear vectores con tamaño negativo o vacio");             
        }
        this.vector = new int[n];
        for (int i = 0; i < vector.length; i++) {
            vector[i] = i;          
        }
    }
    
    public EnteroGrande(int inicio, int fin, boolean esPar){
        if(inicio<=0 || fin<=0 || inicio>=fin){
            throw new RuntimeException("Error de índices");
        }
        this.vector = new int[fin - inicio + 1];
        for (int i = 0; i < vector.length; i++) {
            vector[i] = crearNumero(inicio, fin);
        }
    }
   
    private int crearNumero(int inicio, int fin){
        int n;
        do{
            Random numero = new Random();
            n = numero.nextInt(inicio, fin + 1);
        }while(existe(n));
        return n;
    }
    
    public boolean existe(int x){
        if(this.esVacio()){
            throw new RuntimeException("Está vacío");
        }
        for (int i : vector) {
            if(i == x){
                return true;
            }
        }
        return false;
    }
    
    public boolean esVacio(){
        return this.vector == null;
    }
    //enterogrende 2
    
         public EnteroGrande getUnion(EnteroGrande n2){
        int[] union = new int[vector.length + n2.getVector().length];
         int posicion=0;
         for (int i :  vector) {
         union[posicion]=i;  
         posicion++;
        }
          for (int i : n2.getVector()) {
         union[posicion]=i;  
         posicion++;
        }
          
        EnteroGrande resultado = new EnteroGrande();
        resultado.setVector(union);
        return resultado;
    }
        /*
         */
         
    public EnteroGrande getInterseccion(EnteroGrande n2){
        int posicion=0;
        int tamañoMinimo = Math.min(vector.length, n2.getVector().length);
        int[] interseccion = new int[tamañoMinimo]; 
         while(posicion!=tamañoMinimo){
             
            if(vector.length>=n2.getVector().length){
            for (int i = 0; i < interseccion.length; i++){
            if(n2.getVector()[posicion]==vector[i]){
              interseccion[i] = n2.getVector()[posicion]; 
            }   
           }
          }
          if(vector.length<n2.getVector().length){
            for (int i = 0; i < interseccion.length; i++){
             if(n2.getVector()[i]==vector[posicion]){
               interseccion[i] = vector[posicion]; 
            }   
            }
          }
         
         posicion++;
      }
         EnteroGrande resultado = new EnteroGrande();
        resultado.setVector( interseccion);
        return resultado;
    }
    
    public EnteroGrande getDiferencia(EnteroGrande n2){
        int posicion=0;
        int[] diferencia = new int[vector.length+n2.getVector().length];
        for (int i = 0; i < vector.length; i++) {
        boolean encontrado = false;
        for (int j = 0; j < n2.getVector().length; j++) {
        if (vector[i] == n2.getVector()[j]) {
            encontrado = true;
            break;
        }
    }
        if (!encontrado) {
        diferencia[posicion++] = vector[i];
        }
    }
         for (int i = 0; i < n2.getVector().length; i++) {
         boolean encontrado = false;
         for (int j = 0; j < vector.length; j++) {
         if (n2.getVector()[i] == vector[j]) {
            encontrado = true;
            break;
        }
    }
    if (!encontrado) {
        diferencia[posicion++] = n2.getVector()[i];
        }
    }
     EnteroGrande resultado = new EnteroGrande();
        resultado.setVector( diferencia);
        return resultado;     
    }
    
        
    
      
    @Override
    public String toString() 
{        String mensaje = "";
        for (int i : vector) {
            mensaje += i + "\t";
        }
        return mensaje;
    }    
}
